/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeecustomtable;

/**
 *
 * @author nguyenducthanh
 */
public class CheckValidation {
    public boolean checkCode(String code){
        return code.matches("[S][E]\\d{5}");
    }
    public boolean checkStringDifferNull(String name){
        return !name.matches("");
    }
    public boolean checkSalary(String salary){
        return (salary.matches("\\d+[.]\\d+") || salary.matches("\\d+"));
    }
    
}
