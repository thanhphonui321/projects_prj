/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeecustomtable;

/**
 *
 * @author nguyenducthanh
 */
public class Employee {
    String code, name, address;
    boolean sex;
    double salary;

    public Employee() {
    }

    
    public Employee(String code, String name, String address, boolean sex, double salary) {
        this.code = code;
        this.name = name;
        this.address = address;
        this.sex = sex;
        this.salary = salary;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public boolean isSex() {
        return sex;
    }

    public double getSalary() {
        return salary;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
    
    
}
