/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeecustomtable;

import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author nguyenducthanh
 */
public class EmployeeTableModel <E> extends AbstractTableModel {
    String[] header;
    int[] indexes;
    Vector<Employee> data;

    public EmployeeTableModel(String[] header, int[] indexes) {
        this.header = new String[header.length];
        for (int i = 0; i < header.length; i++){
            this.header[i] = header[i];
        }
        this.indexes = new int[indexes.length];
        for (int i = 0; i < header.length; i++){
            this.indexes[i] = indexes[i];
        }
        this.data = new Vector<Employee>();
    }
    public Vector<Employee> getData(){
        return data;
    }
    
    

    @Override
    public String getColumnName(int columnIndex){
        return header[columnIndex];
    }
    @Override
    public int getRowCount() {
        return data.size();
   }

    @Override
    public int getColumnCount() {
        return header.length;
    }

    @Override
    public Object getValueAt(int row, int column) {
        if (row < 0 || row >= data.size() || column < 0 || column > header.length){
            return null;
        }
        else {
            Employee tmp = data.get(row);
            switch (indexes[column]){
                case 0:
                    return tmp.getCode();
                case 1: 
                    return tmp.getName();
                case 2:
                    return tmp.getAddress();
                case 3: 
                    return tmp.isSex();
                case 4:
                    return tmp.getSalary();
                default:
                    return null;
            }
        }
    }
    
    
    
}
