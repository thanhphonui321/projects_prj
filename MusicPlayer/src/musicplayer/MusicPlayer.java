/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package musicplayer;
import java.io.*;
import java.util.Scanner;
import javazoom.jl.player.Player;

/**
 *
 * @author nguyenducthanh
 */
public class MusicPlayer {
    
    static Player player = null;
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try {
            Scanner sc = new Scanner(System.in);
            String filename = "";
            System.out.println("Enter music file path: ");
            filename = sc.nextLine().trim();
            
            FileInputStream fis = null;
            BufferedInputStream bis = null;
            File f = new File(filename);
            fis = new FileInputStream(f);
            bis = new BufferedInputStream(fis);
            player = new Player(bis);
            
            // run in new thread to play in background
            Thread playingThread = new Thread() {
                public void run(){
                    try {
                        player.play();
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                }
            };
            playingThread.start();
            System.out.println("Music is playing.....");
            System.out.println("Press 'S' to stop! ");
            
            String command = sc.nextLine();
            if (command.equalsIgnoreCase("S")){
                if (player != null){
                    player.close();
                    System.out.println("Player is stoped");
                }
            }
        } catch (Exception e) {
        }
    }
    
}
