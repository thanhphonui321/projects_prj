/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se1272_validator.vn.util.validatior;

/**
 *
 * @author nguyenducthanh
 */
public class InputValidator {
    /**
     * This method check code format. <br>
     * The code format should start with <b>SE</b> or <b>IA</b> or <b>FSB</b> <br>
     * Then followed by exactly 5 digits from 0 to 9 <br>
     * For more information, visit <a href ='http://www.oracle.com'>here</a>
     * @param code the code to be checked
     * @return true/false accordingly
     */
    public static boolean checkCode(String code){
        return code.matches("(SE|IA|FSB)\\d{5}");
    }
    public static boolean checkEmail(String email){
        return email.matches("\\w+@\\w+[.]\\w+([.]\\w+)?");
    }
    public static boolean checkName(String name){
        return name.matches("[\\p{L}\\s]{3,30}");
    }
    
    
}
