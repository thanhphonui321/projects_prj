/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtreeemployee;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;
import javax.swing.JOptionPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreePath;

/**
 *
 * @author nguyenducthanh
 */
public class MainFrame extends javax.swing.JFrame {

    String filename = "department.txt";
    DefaultMutableTreeNode root;
    DefaultMutableTreeNode curDeptNode = null;
    DefaultMutableTreeNode curEmpNode = null;
    boolean addNewDept = true;
    boolean addNewEmp = true;

    /**
     * Creates new form MainFrame
     */
    public MainFrame() {
        initComponents();
        root = (DefaultMutableTreeNode) (this.jTree.getModel().getRoot());
        loadData();
        TreePath path = new TreePath(root);
        jTree.expandPath(path);
    }

    private void loadData() {
        try {
            File file = new File(filename);
            if (!file.exists()) {
                return;
            }
            String details;
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            while ((details = br.readLine()) != null) {
                details = details.trim();
                boolean isDept = (details.charAt(details.length() - 1) == ':');
                StringTokenizer stk = new StringTokenizer(details, "-:,");
                String code = stk.nextToken().trim();
                String name = stk.nextToken().trim();
                if (isDept) {
                    Department dept = new Department(code, name);
                    curDeptNode = new DefaultMutableTreeNode(dept);
                    root.add(curDeptNode);
                } else {
                    int salary = Integer.parseInt(stk.nextToken().trim());
                    Employee emp = new Employee(code, name, salary);
                    curEmpNode = new DefaultMutableTreeNode(emp);
                    curDeptNode.add(curEmpNode);
                }
            }

            curDeptNode = curEmpNode = null;
            br.close();
            fr.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnNewDept = new javax.swing.JButton();
        btnRemoveDept = new javax.swing.JButton();
        btnSaveDept = new javax.swing.JButton();
        txtDeptCode = new javax.swing.JTextField();
        txtDeptName = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtEmpCode = new javax.swing.JTextField();
        txtEmpName = new javax.swing.JTextField();
        txtEmpSalary = new javax.swing.JTextField();
        btnNewEmp = new javax.swing.JButton();
        btnRemoveEmp = new javax.swing.JButton();
        btnSaveEmp = new javax.swing.JButton();
        btnSaveToFile = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTree = new javax.swing.JTree();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Department Details"));

        jLabel1.setText("Dept. code");

        jLabel2.setText("Dept. name");

        btnNewDept.setText("New");
        btnNewDept.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewDeptActionPerformed(evt);
            }
        });

        btnRemoveDept.setText("Remove");
        btnRemoveDept.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveDeptActionPerformed(evt);
            }
        });

        btnSaveDept.setText("Save");
        btnSaveDept.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveDeptActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(btnNewDept)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnRemoveDept)
                .addGap(50, 50, 50)
                .addComponent(btnSaveDept)
                .addGap(19, 19, 19))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtDeptCode, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDeptName, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(102, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDeptCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDeptName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNewDept)
                    .addComponent(btnRemoveDept)
                    .addComponent(btnSaveDept))
                .addContainerGap(39, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Employees Details"));

        jLabel3.setText("Emp. code");

        jLabel4.setText("Emp. name");

        jLabel5.setText("Emp. salary");

        btnNewEmp.setText("New");
        btnNewEmp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewEmpActionPerformed(evt);
            }
        });

        btnRemoveEmp.setText("Remove");
        btnRemoveEmp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveEmpActionPerformed(evt);
            }
        });

        btnSaveEmp.setText("Save");
        btnSaveEmp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveEmpActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 68, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtEmpCode, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtEmpName, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtEmpSalary, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(btnNewEmp)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnRemoveEmp)
                        .addGap(55, 55, 55)))
                .addComponent(btnSaveEmp)
                .addGap(19, 19, 19))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtEmpCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtEmpName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtEmpSalary, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNewEmp)
                    .addComponent(btnRemoveEmp)
                    .addComponent(btnSaveEmp))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnSaveToFile.setText("Save To File");
        btnSaveToFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveToFileActionPerformed(evt);
            }
        });

        jScrollPane1.setVerifyInputWhenFocusTarget(false);

        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("Department");
        jTree.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        jTree.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTreeMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTree);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 283, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(206, 206, 206)
                        .addComponent(btnSaveToFile, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSaveToFile, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jScrollPane1.getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTreeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTreeMouseClicked
        // TODO add your handling code here:
        jTree.cancelEditing();
        TreePath path = jTree.getSelectionPath();
        if (path == null) {
            return;
        }
        DefaultMutableTreeNode selectedNode = null;
        selectedNode = (DefaultMutableTreeNode) (path.getLastPathComponent());
        Object selecObject = selectedNode.getUserObject();
        if (selectedNode == root) {
            this.curDeptNode = this.curEmpNode = null;
        } else {
            if (selecObject instanceof Department) {
                this.curDeptNode = selectedNode;
                this.curEmpNode = null;
            } else if (selecObject instanceof Employee) {
                curEmpNode = selectedNode;
                curDeptNode = (DefaultMutableTreeNode) (selectedNode.getParent());
            }
        }
        viewDeptAndEmp();

    }//GEN-LAST:event_jTreeMouseClicked

    private boolean validDepDetails(Department dept) {

        // check code
        if (!dept.getDeptCode().matches("[A-Z]{2}")) {
            return false;
        }
        for (int i = 0; i < root.getChildCount(); i++) {
            Department tmpDep = (Department) (((DefaultMutableTreeNode) root.getChildAt(i)).getUserObject());
            if (tmpDep.getDeptCode().equals(dept.getDeptCode())) {
                return false;
            }
        }
        // check name

        if (dept.getDeptName().matches("")) {
            return false;
        }
        return true;
    }
    private void btnNewEmpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewEmpActionPerformed
        // TODO add your handling code here:
        txtEmpCode.setText("");
        txtEmpName.setText("");
        txtEmpSalary.setText("");
        txtEmpCode.setEditable(true);
        txtEmpCode.requestFocus();
    }//GEN-LAST:event_btnNewEmpActionPerformed

    private boolean validEmpDetails(Employee emp) {
        if (!emp.getCode().matches("[A-Z][0-9]{3}") || emp.getName().matches("") || emp.getSalary() <= 0) {
            return false;
        }
        for (int i = 0; i < root.getChildCount(); i++) {
            for (int j = 0; j < root.getChildAt(i).getChildCount(); j++) {
                DefaultMutableTreeNode empNode = (DefaultMutableTreeNode) root.getChildAt(i).getChildAt(j);
                Employee tmpEmp = (Employee) empNode.getUserObject();
                if (tmpEmp.getCode().equals(emp.getCode())) {
                    return false;
                }
            }
        }
        return true;
    }
    private void btnSaveEmpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveEmpActionPerformed
        // TODO add your handling code here:
        // check if dep exist
        if (curDeptNode == null) {
            JOptionPane.showMessageDialog(this, "You have to choose Department.");
        } else {
            String empCode = txtEmpCode.getText();
            String empName = txtEmpName.getText();
            int empSalary;
            try {
                empSalary = Integer.parseInt(txtEmpSalary.getText());
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, "uhm uhm, Wrong !!!");
                return;
            }
            Employee tmpEmp = new Employee(empCode, empName, empSalary);
            if (validEmpDetails(tmpEmp)) {
                curDeptNode.add(new DefaultMutableTreeNode(tmpEmp));
            } else {
                JOptionPane.showMessageDialog(this, "uhm uhm, Wrong !!!");
            }
            jTree.updateUI();
        }

        // check valid and add

    }//GEN-LAST:event_btnSaveEmpActionPerformed

    private void btnRemoveEmpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveEmpActionPerformed
        // TODO add your handling code here:
        if (curEmpNode == null) {
            JOptionPane.showMessageDialog(this, "Do you know what're you doing dude ???");
        } else {
            int respone = JOptionPane.showConfirmDialog(this, "Delete this employee ???");
            if (respone == JOptionPane.OK_OPTION) {
                curDeptNode.remove(this.curEmpNode);
                jTree.updateUI();
            }
        }

        curDeptNode = null;
        curEmpNode = null;
    }//GEN-LAST:event_btnRemoveEmpActionPerformed

    private void btnSaveToFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveToFileActionPerformed
        // TODO add your handling code here:
        try {
            PrintWriter pw = new PrintWriter(filename);
            for (int i = 0; i < root.getChildCount(); i++) {
                DefaultMutableTreeNode tmpDepNode = (DefaultMutableTreeNode) root.getChildAt(i);
                Department tmpDepObject = (Department) tmpDepNode.getUserObject();
                pw.println(tmpDepObject.toString() + ":");
                for (int j = 0; j < tmpDepNode.getChildCount(); j++) {
                    DefaultMutableTreeNode tmpEmpNode = (DefaultMutableTreeNode) tmpDepNode.getChildAt(j);
                    Employee tmpEmpObject = (Employee) tmpEmpNode.getUserObject();
                    pw.println(String.format("%s, %s, %d", tmpEmpObject.getCode(), tmpEmpObject.getName()
                    ,tmpEmpObject.getSalary()));
                }
            }
            pw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnSaveToFileActionPerformed

    private void btnSaveDeptActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveDeptActionPerformed
        // TODO add your handling code here:
        String deptCode = txtDeptCode.getText().trim().toUpperCase();
        String deptName = txtDeptName.getText().trim();
        if (validDepDetails(new Department(deptCode, deptName))) {
            root.add(new DefaultMutableTreeNode(new Department(deptCode, deptName)));
        } else {
            JOptionPane.showMessageDialog(this, "uhm uhm, Wrong !!!");
        }
        jTree.updateUI();
    }//GEN-LAST:event_btnSaveDeptActionPerformed

    private void btnRemoveDeptActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveDeptActionPerformed
        // TODO add your handling code here:
        if (this.curDeptNode.getChildCount() > 0) {
            String msg = "Remove all employees before deleting a department.";
            JOptionPane.showMessageDialog(this, msg);
        } else {
            int respone = JOptionPane.showConfirmDialog(this, "Delete this department ???");
            if (respone == JOptionPane.OK_OPTION) {
                root.remove(this.curDeptNode);
                jTree.updateUI();
            }
        }
        curDeptNode = null;
        curEmpNode = null;
    }//GEN-LAST:event_btnRemoveDeptActionPerformed

    private void btnNewDeptActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewDeptActionPerformed
        // TODO add your handling code here:

        txtDeptCode.setText("");
        txtDeptName.setText("");
        txtEmpCode.setText("");
        txtEmpName.setText("");
        txtEmpSalary.setText("");
        txtDeptCode.setEditable(true);
        txtDeptCode.requestFocus();
    }//GEN-LAST:event_btnNewDeptActionPerformed

    private void viewDeptAndEmp() {
        Department curDep = null;
        Employee curEmp = null;
        if (curDeptNode != null) {
            curDep = (Department) curDeptNode.getUserObject();

        }
        if (curEmpNode != null) {
            curEmp = (Employee) curEmpNode.getUserObject();

        }
        this.txtDeptCode.setText(curDep != null ? curDep.getDeptCode() : "");
        this.txtDeptName.setText(curDep != null ? curDep.getDeptName() : "");
        this.txtEmpCode.setText(curEmp != null ? curEmp.getCode() : "");
        this.txtEmpName.setText(curEmp != null ? curEmp.getName() : "");
        this.txtEmpSalary.setText("" + (curEmp != null ? curEmp.getSalary() : ""));
        this.txtDeptCode.setEditable(false);
        this.txtEmpCode.setEditable(false);

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                MainFrame myFrame = new MainFrame();
                myFrame.setLocationRelativeTo(myFrame);
                myFrame.setVisible(true);
                
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnNewDept;
    private javax.swing.JButton btnNewEmp;
    private javax.swing.JButton btnRemoveDept;
    private javax.swing.JButton btnRemoveEmp;
    private javax.swing.JButton btnSaveDept;
    private javax.swing.JButton btnSaveEmp;
    private javax.swing.JButton btnSaveToFile;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTree jTree;
    private javax.swing.JTextField txtDeptCode;
    private javax.swing.JTextField txtDeptName;
    private javax.swing.JTextField txtEmpCode;
    private javax.swing.JTextField txtEmpName;
    private javax.swing.JTextField txtEmpSalary;
    // End of variables declaration//GEN-END:variables
}
