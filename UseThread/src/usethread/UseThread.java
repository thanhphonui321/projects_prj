/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usethread;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nguyenducthanh
 */
public class UseThread {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Thread t = new Thread(new ImpleRunnable());
        t.start();
        for (int i = 0; i < 10; i++){
            try {
                Thread.currentThread().setPriority(10);
                System.out.println(Thread.currentThread().getId() + " number " + i + ", priority = " + Thread.currentThread().getPriority());
                Thread.sleep(1000);
            }
//        System.out.println("I'm the main thread");
            catch (InterruptedException ex) {
                Logger.getLogger(UseThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
    
}
