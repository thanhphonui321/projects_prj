/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usethread;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nguyenducthanh
 */
public class ImpleRunnable implements Runnable {

    public ImpleRunnable() {

    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            try {
                System.out.println(Thread.currentThread().getId() + " number " + i + ", priority = " + Thread.currentThread().getPriority());
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Thread1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
