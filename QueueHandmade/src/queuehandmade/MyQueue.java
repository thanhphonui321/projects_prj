/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package queuehandmade;

/**
 *
 * @author nguyenducthanh
 */
public class MyQueue<T> {

    private SllManagement<T> s = new SllManagement<>();

    public MyQueue() {

    }
    public boolean isEmpty(){
        return s.head == null;
    }
    public void enqueue(T el) {
        s.addLast(el);
    }

    public T dequeue() {
        if (s.isEmpty()) {
            throw new java.util.EmptyStackException();
        }
        T tmpEl = s.get(0);
        s.removeFirst();
        return tmpEl;

    }

    public T front() {
        if (s.isEmpty()) {
            throw new java.util.EmptyStackException();
        }

        return s.get(0);
    }
}
