/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package queuehandmade;

/**
 *
 * @author nguyenducthanh
 */
public class SllNode<T> {

    public T info;
    public SllNode<T> next;

    public SllNode() {
        next = null;
    }

    public SllNode(T vl) {
        info = vl;
        next = null;
    }

    public SllNode(T vl, SllNode n) {
        info = vl;
        next = n;
    }
}