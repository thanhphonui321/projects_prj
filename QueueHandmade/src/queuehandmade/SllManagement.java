/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package queuehandmade;

/**
 *
 * @author nguyenducthanh
 */
public class SllManagement<T> {

    public SllNode<T> head, tail;

    public SllManagement() {
        head = tail = null;

    }

    // size
    public int size() {
        SllNode<T> q = head;
        int size = 0;
        while (q != null) {
            size++;
            q = q.next;

        }
        return size;
    }

    // check empty
    public boolean isEmpty() {
        return head == null;
    }

    // add Last
    public void addLast(T vl) {
        SllNode<T> q = new SllNode<>(vl);
        if (!isEmpty()) {
            tail.next = q;
            tail = q;
        } else {
            head = tail = q;
        }
    }

    // add First
    public void addFirst(T vl) {
        SllNode<T> q = new SllNode<>(vl);
        if (isEmpty()) {
            head = tail = q;
        } else {
            q.next = head;
            head = q;
        }
    }

    // print All
    public void printAll() {
        if (isEmpty()) {
            System.out.println("No elements.");
        } else {
            SllNode<T> q = head;
            while (q != null) {
                System.out.println(q.info);
                q = q.next;
            }
        }

    }

    // add to Position
    public void addPos(int pos, T value) {
        if (pos == 0) {
            addFirst(value);
        } else {
            try {
                SllNode<T> q = new SllNode<>(value);

                // set countBefore and countAfter
                SllNode<T> countBefore = head;
                SllNode<T> countAfter;
                for (int i = 0; i < pos - 1; i++) {
                    countBefore = countBefore.next;
                }
                countAfter = countBefore.next;

                // Add to pos
                countBefore.next = q;
                q.next = countAfter;
                System.out.println("Added");
            } catch (NullPointerException ex) {
                System.out.println("Wrong position.");
            }

        }

    }

    // Remove First item
    public void removeFirst() {
        try {
            head = head.next;
        } catch (NullPointerException ex) {
            System.out.println("Failed");
        }

    }

    // remove last item
    public void removeLast() {
        try {
            switch (size()) {
                case 0:
                    throw new Exception();
                case 1:
                    head = tail = null;
                    break;
                default:
                    SllNode<T> count = head;
                    for (int i = 1; i <= size() - 2; i++) {
                        count = count.next;
                    }   tail = count;
                    tail.next = null;
                    break;
            }
        } catch (Exception ex) {
            System.out.println("Failed");
        }

    }

    // add an array
    public void addMany(T[] a) {
        for (T a1 : a) {
            addLast(a1);
        }
    }

    // get value
    public T get(int pos) {
        SllNode<T> q = head;
        for (int i = 0; i < pos; i++) {
            q = q.next;
        }
        return q.info;

    }

    // get index
    public int indexOf(T vl) {
        try {
            SllNode<T> q = head;
            int count = 0;
            for (int i = 0; i < size(); i++) {
                if (q.info == vl) {
                    return count;
                }
                q = q.next;
                count++;

            }
        } catch (NullPointerException ex) {
        }

        return -1;
    }

    //remove position
    public void remove(int pos) {
        try {
            if (pos == 0) {
                head = head.next;
            } else {
                SllNode<T> q = head;
                for (int i = 1; i < pos; i++) {
                    q = q.next;
                }
                q.next = q.next.next;
            }
        } catch (NullPointerException ex) {
            System.out.println("Failed");
        }

    }

    // clear all
    public void clearAll() {
        head = tail = null;
    }

    // reverse 
    public void reverse() {

        int tmpSize = size();
        if (!isEmpty() && size() > 1) {

            // change head and tail
            SllNode<T> tmp = head;
            head = tail;
            tail = tmp;

            // Situation
            switch (tmpSize) {
                case 2:
                    tail.next = null;
                    head.next = tail;
                    break;
                case 3:
                    head.next = tail.next;
                    tail.next.next = tail;
                    tail.next = null;
                    break;
                default:
                    try {
                        SllNode<T> q1;
                        SllNode<T> q2;
                        SllNode<T> q3;

                        // Set q1, q2, q3
                        q1 = tail.next;
                        q2 = q1.next;
                        q3 = q2.next;

                        // set initial value
                        tail.next = null;
                        q1.next = tail;

                        // loop
                        while (q1 != head && q2 != head && q3 != head) {
                            q2.next = q1;
                            q1 = q3.next;
                            if (q1 == head) {
                                break;
                            }
                            q3.next = q2;
                            q2 = q1.next;
                            if (q2 == head) {
                                break;
                            }
                            q1.next = q3;
                            q3 = q2.next;
                            if (q3 == head) {
                                break;
                            }
                        }

                        if (q1 == head) {
                            q3.next = q2;
                            q1.next = q3;
                        } else if (q2 == head) {
                            q1.next = q3;
                            q2.next = q1;
                        } else if (q3 == head) {
                            q2.next = q1;
                            q3.next = q2;
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    break;
            }

        }

    }

    // check 2 singly linked list have the same contents
    public boolean haveSameContents(SllManagement sll) {

        if (this.size() != sll.size()) {
            return false;
        } else {
            for (int i = 0; i < size(); i++) {
                if (!this.get(i).equals(sll.get(i))) {
                    return false;
                }
            }
        }
        return true;
    }
}
