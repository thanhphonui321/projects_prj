/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cooltextfield;

import java.awt.Graphics;
import javax.swing.Icon;
import javax.swing.JTextField;

/**
 *
 * @author nguyenducthanh
 */
public class CoolTextField extends JTextField{

    private Icon icon;
    public CoolTextField(){
        super();
        this.icon = null;
    }
    
    

    public void setIcon(Icon icon) {
        this.icon = icon;
    }
    
    public Icon getIcon(){
        return this.icon;
    }
    @Override
    protected void paintComponent(Graphics g){
        super.paintComponent(g);
        if (this.icon != null){
            int iconHeight = icon.getIconHeight();
            int y = (this.getHeight() - iconHeight)/2;
            icon.paintIcon(this, g, this.getWidth() - 20, y);
        }
    }
    
}
