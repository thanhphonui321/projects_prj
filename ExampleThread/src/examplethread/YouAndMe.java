/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examplethread;

/**
 *
 * @author nguyenducthanh
 */
public class YouAndMe implements Runnable {

    private BankAccount account = new BankAccount();
    
     public static void main(String[] args) {
        // TODO code application logic here
        YouAndMe job = new YouAndMe();
        Thread one = new Thread(job);
        Thread two = new Thread(job);
        one.setName("You");
        two.setName("Me");
        one.start();
        two.start();
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            makeWithdraw(10);
            if (account.getBalance() < 0) {
                System.out.println(" Overdrawn");
            }
        }
    }

    public void makeWithdraw(int amount) {
        if (account.getBalance() < amount) {
            System.out.println(" Sorry, not enough for " + Thread.currentThread().getName());
        } else {
            System.out.println(Thread.currentThread().getName() + " is about to withdraw");

            try {
                System.out.println(Thread.currentThread().getName() + " is going to sleep");
                Thread.sleep(500);
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " woke up");
            account.withdraw(10);
            System.out.println(Thread.currentThread().getName() + " completes the withdraw");
        }
    }
}
