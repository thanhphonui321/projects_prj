/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nonraceproblem;

/**
 *
 * @author nguyenducthanh
 */
public class RaceThread extends Thread{
    int no;
    Printer printer;

    public RaceThread(int no, Printer printer) {
        this.no = no;
        this.printer = printer;
    }

    @Override
    public void run() {
        printer.printNumber(no);
    }
    
    
}
