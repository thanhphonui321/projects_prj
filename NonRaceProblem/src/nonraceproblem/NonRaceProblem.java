/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nonraceproblem;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nguyenducthanh
 */
public class NonRaceProblem {

    Date d = null;
    long num1 = 0, num2 = 0;

    public NonRaceProblem() {
        d = new Date(System.currentTimeMillis());
        randomNumbers();
        new TimeThread().start();
        new AddThread().start();
    }

    void randomNumbers() {
        num1 = Math.round(Math.random() * 1000);
        num2 = Math.round(Math.random() * 1000);
    }

    class TimeThread extends Thread {

        TimeThread() {
            super();
        }

        @Override
        public void run() {
            while (true) {
                try {

                    System.out.println(d);
                    Thread.sleep(1000);
                    d = new Date(System.currentTimeMillis());
                } catch (Exception ex) {

                }
            }
        }

    }

    class AddThread extends Thread {

        public AddThread() {
            super();
        }

        @Override
        public void run() {
            while (true) {
                try {

                    System.out.println(num1 + num2);

                    Thread.sleep(1000);
                    randomNumbers();

                } catch (Exception ex) {

                }
            }
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
///        NonRaceProblem x = new NonRaceProblem();

        RaceThread[] races = new RaceThread[10];
        Printer printer = new Printer();

        //creating and assinging resource 'printer' to 10 threads
        for (int i = 0; i < 10; i++) {
            races[i] = new RaceThread(i, printer);
        }

        // starting 10 threads
        for (int i = 0; i < 10; i++) {
            races[i].start();
        }

    }
}
